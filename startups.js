var pageTop = document.getElementById("page-top");

if (pageTop !== null) {
  var urlSponsor =
    "https://bitbucket.org/openexchange-corsight/openexchange-coresight.bitbucket.io/raw/master/startups.json";
  fetch(urlSponsor)
    .then(function (response) {
      // The API call was successful!
      return response.json();
    })
    .then(function (data) {
      // This is the JSON from our response
      var urlAuthSuffix = document.getElementById("url-auth-suffix");
      if (urlAuthSuffix !== null) {
        var valueUrlAuthSuffix = urlAuthSuffix.getAttribute("data-value");

        if (data.sponsors.length) {
          var sponsors = data.sponsors;
          var sponsorsHTML = "<div class='container' id='startup-container'>";
          sponsorsHTML +=
            "<div class='startup-header'><h3>" +
            data.sponsorHeader +
            "</h3></div>"; // sponsors header html

          // sponsor element include image and name
          sponsorsHTML += "<div class='startup-wrapper'>";
          sponsors.forEach((sponsor) => {
            sponsorsHTML +=
              "<div class='startup-element element-" + sponsor.id + "'>";
            sponsorsHTML +=
              "<div><a href='" +
              sponsor.url +
              "' title='" +
              sponsor.name +
              "' target='_blank'><img src='" +
              sponsor.img +
              "'></a></div>"; // img container
            sponsorsHTML += "</div>";
          });
          sponsorsHTML += "</div>";
          sponsorsHTML += "</div>";
          // asign sponsorHtml to startup container and append the sponsor container to page-top
          var sponsorContainerDiv = document.createElement("div"); // make div
          sponsorContainerDiv.id = "startup-sponsor";
          sponsorContainerDiv.innerHTML = sponsorsHTML;
          pageTop.appendChild(sponsorContainerDiv);
        }

        if (data.startups.length) {
          var startups = data.startups;
          var startupsHTML = "<div class='container' id='startup-container'>";
          startupsHTML +=
            "<div class='startup-header'><h3><a href='" +
            data.startupsUrl +
            valueUrlAuthSuffix +
            "'>" +
            data.startupHeader +
            "</a></h3></div>"; // startups header html

          // startup element include image and name
          startupsHTML += "<div class='startup-wrapper'>";
          startups.forEach((startup) => {
            startupsHTML +=
              "<div class='startup-element element-" + startup.id + "'>";
            startupsHTML +=
              "<a href='" + data.startupsUrl + valueUrlAuthSuffix + "'>";
            startupsHTML +=
              "<div class='startup-img-wrapper'><img src='" +
              startup.img +
              "'></div>"; // img container
            startupsHTML +=
              "<div class='startup-name-wrapper'><p>" +
              startup.name +
              "</p></div>"; // name container
            startupsHTML += "</a>";

            startupsHTML += "</div>";
          });
          startupsHTML += "</div>";
          startupsHTML += "</div>";
          // asign startupsHTML to startup container and append the sponsor container to page-top
          var sponsorContainerDiv = document.createElement("div"); // make div
          sponsorContainerDiv.id = "startup";
          sponsorContainerDiv.innerHTML = startupsHTML;
          pageTop.appendChild(sponsorContainerDiv);
        }
      }
    })
    .catch(function (err) {
      // There was an error
      console.warn("Something went wrong.", err);
    });
}

if (window.location.href.indexOf('login') > 0) {
  console.log("The URL contains login");
  window.location.href="https://coresightevents-livestreamshopping2021.videoshowcase.net/";
}
